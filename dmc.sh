#!/bin/bash

blue='\033[0;34m';
cyan='\033[0;36m';
yellow='\033[0;33m';
red='\033[0;31m';
nocol='\033[0m';

echo -e "$cyan@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "$blue@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@$nocol";sleep 0.2;
echo -e "@@@@@@@@@@ SCRIPT KERNEL COMPILER BY YUYUN RYUZAKI @@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@         !!!!!!ARM ONLY!!!!!!!!!         @@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@                                         @@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@                                         @@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@   SEBELUM MELAKUKAN HARAP MELENGKAPI    @@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@         PAKET DAHULU ,KUNJUNGI          @@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@     https://pastebin.com/u/yukenz       @@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@        UNTUK INFO  ERROR-DSB            @@@@@@@@@@";sleep 0.2;
echo -e "$red@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "$yellow@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "$nocol";

echo -ne "$cyan*Nama build usernya (ex:YukenZ)(no space) : ";read buser;
echo -ne "*Nama build hostnya (ex:Debian9)(no space) : ";read bhost;
echo -ne "*Masukan path folder kernelnya sourcenya (ex:kernel) : ";read pfk;
echo -ne "*Apa nama defconfig kernel yang mau kamu compile (liat di <kernel source>/arch/arm/configs/*) : ";read kcf;
echo -ne "*kamu punya $(nproc --all ;) cpu,mau kamu gunakan berapa (ex:2): ";read cpuc;
echo -ne "*ZIP Package version or name (ex:REV11) : ";read z;
echo -ne "*Kernel build number (ex:11.09) : ";read bn;
echo -ne "*kernel mu mau kamu namakan apa (ex:RIBELLION) : ";read nker;
echo -ne "$nocol";

#platform specifies
export KBUILD_BUILD_USER=$buser;
export KBUILD_BUILD_HOST=$bhost;
export ARCH=arm;
export SUBARCH=arm;
TOOL_CHAIN_ARM=arm-eabi-;
export USE_CCACHE=1;
#masukan path kernel toolchain disini
KERNEL_TOOLCHAIN=$PWD/UBERTC-arm-eabi-4.9-94cfd739eed6/bin/$TOOL_CHAIN_ARM;
KERNEL_DIR=$PWD/$pfk;
cd $KERNEL_DIR;
DT_IMG=$KERNEL_DIR/arch/arm/boot/dt.img;
DTBTOOL=$KERNEL_DIR/tools/dtbToolCM;
BUILD_START=$(date +"%s");

#KERNEL_DEFCONFIG=$kcf;
MAKE_JOBS=$cpuc;
export CROSS_COMPILE=$KERNEL_TOOLCHAIN;

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    clean)
    CLEAN_BUILD=YES
    #shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

rm -f $KERNEL_DIR/.version;
	echo 0 > $KERNEL_DIR/.version;
	rm -f $KERNEL_DIR/include/generated/compile.h;

EV=EXTRAVERSION=-$bn-;
  echo "$bn" > $KERNEL_DIR/.extraversion;

## MEMULAI COMPILE ##
echo "" ;
echo -e "$red@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "####################### SCRIPT COMPILER #####################";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@$nocol";sleep 0.2;
echo " ";sleep 0.2;
echo "***** Tool chain is set to *****";sleep 0.2;
echo "$KERNEL_TOOLCHAIN";sleep 0.2;
echo "";sleep 0.2;
echo "***** Kernel defconfig is set to *****";sleep 0.2;
echo "$KERNEL_DEFCONFIG";sleep 0.2;
echo "";sleep 0.2;
echo "" ;
echo -e "$red@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "######################### MAKE KDEF #########################";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@$nocol";sleep 0.2;
echo " ";sleep 0.2;
make $KERNEL_DEFCONFIG menuconfig;
make;

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
# Read [clean]
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
if [ "$CLEAN_BUILD" == 'YES' ]
        then
        echo;
        echo "***************************************************************";
        echo "***************!!!!!  BUILDING CLEAN  !!!!!********************";
        echo "***************************************************************";
        echo;
         make clean;
         make mrproper;
        make ARCH=$ARCH CROSS_COMPILE=$TOOL_CHAIN_ARM  $KERNEL_DEFCONFIG;
fi

v=`cat .extraversion`;
EV=EXTRAVERSION=--$v-;
make $EV -j$MAKE_JOBS;

ANYKERNEL=./RBL/AnyKernel2;
KERNELPATH=$KERNEL_DIR/arch/arm/boot;
ZIMAGE=zImage

DATE=$(date +"%Y%m%d-%H%M");

ZIP=$nker-$z-redmi2-$DATE.zip;
cd ..;

echo "" ;
echo -e "$red@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "#######################    MAKING DTB   #####################";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@$nocol";sleep 0.2;
echo " ";sleep 0.2;
chmod -R 755 RBL;
./RBL/dtbToolCM -2 -o $KERNEL_DIR/arch/arm/boot/dt.img -s 2048 -p $KERNEL_DIR/scripts/dtc/ $KERNEL_DIR/arch/arm/boot/dts/;
cp -r $KERNEL_DIR/arch/arm/boot/dt.img $ANYKERNEL/dtb

# Create flashable zip
if [ -f $KERNELPATH/$ZIMAGE ]
then
echo "" ;
echo -e "$red@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e "############ CREATED FLASHABLE ZIP BY AnyKernel2 ############";sleep 0.2;
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@";sleep 0.2;
echo -e  "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@$nocol";sleep 0.2;
echo " ";sleep 0.2;
cp -f $KERNELPATH/$ZIMAGE $ANYKERNEL/zImage;
cd $ANYKERNEL/;
zip -qr9 $ZIP .;
cd ../..;
echo "ZIP MU DI $PWD/$ZIP" ;
mv -f $ANYKERNEL/$ZIP $PWD/$ZIP;
echo "Doing post-cleanup...";
rm -f $KERNELPATH/$ZIMAGE;
rm -f $ANYKERNEL/zImage;
rm -f $ANYKERNEL/dtb;
echo "Done.";

BUILD_END=$(date +"%s");
DIFF=$(($BUILD_END - $BUILD_START));

echo "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#";
echo "                                        ";
echo "     KERNEL BUILD IS SUCCESSFUL         ";
echo "                                        ";
echo " $ZIP                 ";
echo "                                        ";
echo -e "$yellow Build completed in $(($DIFF / 60)) minute(s) and $(($DIFF % 60)) seconds.$nocol";
echo "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#";
else
echo "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#";
echo "                                        ";
echo "     ERROR !!! ERROR !!! ERROR !!!      ";
echo "                                        ";
echo "          DON'T GIVE UP @_@             ";
echo "                                        ";
echo "#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#";
fi
exit
